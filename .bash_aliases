alias bat='docker run -it --rm -e BAT_THEME -e BAT_STYLE -e BAT_TABS -v "$(pwd):/myapp" danlynn/bat'
alias mgrep='grep -riIn --color'
alias mgrepc='grep -riIn --include \*.h --include \*.c --include \*.cpp'
