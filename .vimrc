syntax enable
filetype on
filetype plugin indent on

" Remap leader key
:let mapleader = " "

" Insert 4 spaces when tab is pressed, and 4 spaces for indention
set tabstop=4 shiftwidth=4 expandtab

" use indentation of previous line, and take into account current coding
" syntax
set autoindent smartindent cindent

" Good Backspacing
set backspace=indent,eol,start

set number 
set showcmd
set cursorline
set wildmenu
set showmatch
set nohlsearch

" Global path option. Files that are children within a project will be
" locatable
setlocal path=.,**

" No swap files
set noswapfile 

" Automatically update file when it changes on disk
set autoread
au CursorHold * checktime  

" Searching bits. Case insensitive seach. Smartcase will do a case 
" senstive search if using caps
set incsearch
set ignorecase
set smartcase

filetype indent on

" Remove vim's status line - competing with vim airline
set laststatus=0
set cmdheight=1
set noruler

" set UTF-8 encoding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

" disable vi compatibility (emulation of old bugs)
set nocompatible

" Use tab for auto completetion
let g:SuperTabDefaultCompletionType = "<C-X><C-O>"

"Disable arrow keys. Practice makes perfect!
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

"Use ctrl hjkl to navigate in insert mode
inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-l> <Right>

"Use ctrl hjkl to navigate in command mode
cnoremap <C-h> <Left>
cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
cnoremap <C-l> <Right>

"
" Clipboard stuff
"
if !exists("g:os")
    if has("win64") || has("win32") || has("win16")
        let g:os = "Windows"
    else
        let g:os = substitute(system('uname'), '\n', '', '')
    endif
endif

if has('clipboard')
    if g:os == "Darwin"
        set clipboard=unnamed
    elseif g:os == "Linux"
        if has('unnamedplus')
            set clipboard=unnamedplus
        else
            set clipboard=unnamed
        endif
    elseif g:os == "Windows"
        set clipboard=unnamed
    endif
endif

"Automatic toggling between relative and regular line number
:set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

" Get to normal mode with jj instead 
:imap jj <Esc>

" Navigate Panes 
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Save with ctrl+s to save files
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>i

"          -- Ctags -- 
set tags+=~/.vim/tags/cpp
set tags+=tags


" Build tags for current project with ctrl+F12
map <C-F12> :!ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q .<CR>

" Folds
" unmap find chara and map instead to fold and unfold blocks
nnoremap ff zf%
nnoremap fu zd

"Move a highlight block of text around with shift k/j
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '>-2<CR>gv=gv

" Undo Break Points
" Instead of vim erasing an entire line you you typed, it will stop
" At each of these break points.
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u

function! Marks()
    marks
    echo('Mark: ')

    " getchar() - prompts user for a single character and returns the chars
    " ascii representation
    " nr2char() - converts ASCII `NUMBER TO CHAR'

    let s:mark = nr2char(getchar())
    " remove the `press any key prompt'
    redraw

    " build a string which uses the `normal' command plus the var holding the
    " mark - then eval it.
    execute "normal! '" . s:mark
endfunction

nnoremap <silent> ' :call Marks()<CR>

"
"########### Plugins Specific Stuffs #############
"
"     -- NerdTree --
"Toggle NERDtree to Ctrl+N
nmap <C-N> :NERDTreeToggle<CR>
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let NERDTreeShowBookmarks=1

"     -- Cscope --
if has("cscope")

    """"""""""""" Standard cscope/vim boilerplate
    set csto=1

    " show msg when any other cscope db added
    set cscopeverbose  

    " Open results in quickfix window
    set cscopequickfix=s-,c-,d-,i-,t-,e-

    " The following maps all invoke one of the following cscope search types:
    "
    "   's'   symbol: find all references to the token under cursor
    "   'g'   global: find global definition(s) of the token under cursor
    "   'c'   calls:  find all calls to the function name under cursor
    "   't'   text:   find all instances of the text under cursor
    "   'e'   egrep:  egrep search for the word under cursor
    "   'f'   file:   open the filename under cursor
    "   'i'   includes: find files that include the filename under cursor
    "   'd'   called: find functions that function under cursor calls
    "
    nmap gd :cs find g <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>	
    nmap <Leader>\s :cs find s <C-R>=expand("<cword>")<CR><CR>	
    nmap <Leader>\g :cs find g <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>	
    nmap <Leader>\c :cs find c <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>	
    nmap <Leader>\t :cs find t <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>	
    nmap <Leader>\e :cs find e <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>	
    nmap <Leader>\f :cs find f <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
    nmap <Leader>\i :cs find i <C-R>=expand("<cword>")<CR><CR>	
    nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>	
    nmap <Leader>\d :cs find d <C-R>=expand("<cword>")<CR><CR>	

    noremap <F4> :<C-U>cnext<CR>
    noremap <S-F4> :<C-U>cprevious<CR>
endif

"      -- SearchTasks --
" Keywords for searchtask to look for
let g:searchtasks_list=["TODO", "FIXME", "XXX"]

"       -- TagBar --
" Map the tagbar to F8
nmap <F8> :TagbarToggle<CR>

"        -- FZF --
" Search for files with ctrl-f
" Search within files using ripgrep with ctrl-g
" Search only for files that git is tracking with ctrl-p
" Search buffer with ctrl-b
" Search lines in current buffer with ctrl-l
" Search Marks with ctrl-m'
" Search buffer history with leader<h>
" Search command history with control-h
" Search search history with leader <hs>
" Search project tags with leader t
nnoremap <silent> <C-f> :Files<Cr>
nnoremap <silent> <C-g> :Rg<CR>
nnoremap <silent> <C-p> :GFiles<Cr>
nnoremap <silent> <C-b> :Buffers<Cr>
nnoremap <silent> <C-l> :BLines<CR>
nnoremap <silent> <C-m> :Marks<CR>
nnoremap <silent> <Leader>hf :History<CR>
nnoremap <silent> <C-h> :History:<CR>
nnoremap <silent> <Leader>hs :History/<CR>
nnoremap <silent> <Leader>t :Tags<CR>

" Determine how to open a secltion
let g:fzf_action = {
      \ 'ctrl-t': 'tab split',
      \ 'ctrl-h': 'split'}

" [Tags] Command to generate tags file
let g:fzf_tags_command = 'ctags -R'

" -- VimWiki
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

" -- VimBookmarks
nmap <Leader><Leader> <Plug>BookmarkToggle
nmap <Leader>nn <Plug>BookmarkAnnotate
nmap <Leader>ns <Plug>BookmarkShowAll
nmap <Leader>j <Plug>BookmarkNext
nmap <Leader>k <Plug>BookmarkPrev
nmap <Leader>nd <Plug>BookmarkClear

" --           Vimux
" -- Run a command within a buffer
map <Leader>rc :VimuxPromptCommand<CR>

" -- Vim Airline
let g:airline_powerline_fonts = 1
function! AirlineInit()
    let g:airline_section_a = airline#section#create(['mode'])
    let g:airline_section_b = airline#section#create(['%f'])
    let g:airline_section_c = airline#section#create([''])
    let g:airline_section_x = airline#section#create(['branch'])
    let g:airline_section_y = airline#section#create(['Line:%l Column:%c'])
    let g:airline_section_z = airline#section#create([''])
    let g:airline_section_error = airline#section#create([''])
    let g:airline_section_warning = airline#section#create([''])
"    let g:airline_section_gutter = airline#section#create([''])
endfunction
autocmd VimEnter * call AirlineInit()
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme = 'minimalist'

"     -- Vim Tasklist ---
map <leader>tl <Plug>TaskList

let g:tlWindowPosition = 1
let g:tlTokenList = ['TODO', 'FIXME']
let g:tlRememberPosition = 1

"     -- Buffergator --
nnoremap <silent> <Leader>T :BuffergatorTabsOpen

"     -- Nerdtree Git --
let g:NERDTreeGitStatusUseNerdFonts = 1

"        -- VimPlug --
" Install vim-plug if it is not installed
if empty(glob('~/.vim/autoload/plug.vim'))
          silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
              \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
            autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"        -- Vim Rooter --
let g:rooter_patterns = ['.git']

"        -- Vim Commentary --
" Comment Paragraph
nmap <Leader>cp gcap

"       -- LeetCode --
"
" Preferred Language. Options: cpp, python, python3, golang, rust
let g:leetcode_solution_filetype = 'python3'

" Select Browser. Options: disabled, chrome, firefox
let g:leetcode_browser = 'chrome'

nnoremap <leader>ll :LeetCodeList<cr>
nnoremap <leader>lt :LeetCodeTest<cr>
nnoremap <leader>ls :LeetCodeSubmit<cr>
nnoremap <leader>li :LeetCodeSignIn<cr>

" Plugins to be installed 
call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'xolox/vim-misc'
Plug 'brookhong/cscope.vim'
Plug 'jeetsukumaran/vim-buffergator'
Plug 'chauncey-garrett/vim-tasklist'
Plug 'majutsushi/tagbar'
Plug 'simeji/winresizer'
Plug 'powerline/powerline'
Plug 'hashivim/vim-vagrant'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'zivyangll/git-blame.vim'
Plug 'davidhalter/jedi-vim'
Plug 'kergoth/vim-bitbake'
Plug 'gcmt/taboo.vim'
Plug 'airblade/vim-gitgutter'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'christoomey/vim-tmux-navigator'
Plug 'vimwiki/vimwiki'
Plug 'chazy/dirsettings'
Plug 'MattesGroeger/vim-bookmarks'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
Plug 'benmills/vimux'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'ryanoasis/vim-devicons'
Plug 'airblade/vim-rooter'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'preservim/nerdcommenter'
Plug 'ianding1/leetcode.vim'

" Themes to be installed
Plug 'vim-scripts/dante.vim'
Plug 'gkjgh/cobalt'
Plug 'yuqio/vim-darkspace'
Plug 'endel/vim-github-colorscheme'
Plug 'bluz71/vim-nightfly-guicolors'
Plug 'morhetz/gruvbox'
Plug 'ayu-theme/ayu-vim'
Plug 'sainnhe/everforest'
Plug 'kshenoy/vim-signature'
call plug#end()

" Set Colorscheme
"silent! set background=dark
silent! set termguicolors
set termguicolors     " enable true colors support

" Everforest Settings
if has('termguicolors')
  set termguicolors
endif

set background=dark
" Set contrast.
" This configuration option should be placed before `colorscheme everforest`.
" Available values: 'hard', 'medium'(default), 'soft'
let g:everforest_background = 'medium'
colorscheme everforest
